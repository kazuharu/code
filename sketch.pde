//2018 6/30作成
//ライブラリ-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
import java.util.Arrays;
import java.util.Random;

//変数----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int BasicX = 10;//図形を表示するとき起点にするX座標。
int BasicY = 10;//図形を表示するときに起点にするY座標。
int ScreenXHigh;//スクリーンに表示できる最大のX座標。
int ScreenYHigh;//スクリーンに表示できる最大のY座標。
int PlayerSpeed = 2;//プレイヤーのスピード。
int DownKeyPush = 0;//下矢印キーの状態 1 = HIGH
int UpKeyPush = 0;//上矢印キーの状態 1 = HIGH
int RightKeyPush = 0;//右矢印キーの状態 1 = HIGH
int LeftKeyPush = 0;//左矢印キーの状態 1 = HIGH
ArrayList<Integer> FoodsX = new ArrayList<Integer>();//食べ物のX座標を管理する配列。
ArrayList<Integer> FoodsY = new ArrayList<Integer>();//食べ物のY座標を管理する配列。
ArrayList<Integer> FoodsColor = new ArrayList<Integer>();//食べ物のいろを管理する関数。(1 = 青,2 = 赤,3 = 青)

//関数----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void setup(){//起動する際に1回呼び出される関数。


  size(800,600);//ウィンドウサイズを定義。
  
  
  for(int i = 0; i < 700;i += 1){//食べ物の位置をランダムに決定.700個生成される。
    Random rand = new Random();//乱数を生成。
    FoodsX.add(rand.nextInt(3500)); //X座標 0〜-3500までの範囲で決定。
    Random rand2 = new Random(); //乱数を生成
    FoodsY.add(rand2.nextInt(3500));//Y座標0〜-3500までの範囲で決定。
    Random rand3 = new Random();//乱数を生成
    FoodsColor.add(rand3.nextInt(3));//食べ物のいろを三色の中から決定。
    //それぞれの変数に対応するインデックスは一つの食べ物の情報。
  }//ループ終了
  
}//関数終了


void PlayerMove(){//プレイヤーの位置情報を変更。


  if (UpKeyPush == 1){//上矢印キーを押されている間動作する。
    BasicY += PlayerSpeed;
  }
  if (DownKeyPush == 1){//下矢印キーを押されている間動作する。
    BasicY -= PlayerSpeed;
  }
  if (RightKeyPush == 1){//右矢印キーを押されている間動作する。
    BasicX -= PlayerSpeed;
  }
  if (LeftKeyPush == 1){//左矢印キーを押されている間動作する。
    BasicX += PlayerSpeed;
  }
  
  
}//関数終了


void draw(){//1フレームごとにループする関数


  background(255);
  colorMode(RGB,256);//画面をリフレッシュ。
  fill(240,20,20);//プレイヤーの色を定義。
  
  
  for(int i = 0; i < FoodsX.size(); i += 1){//食べ物の数の回数ループする。
  
  
    if (FoodsColor.get(i) == 0){ //食べ物の色を食べ物の色データから取得して塗り分ける。
     fill(245,27,31);
    }if (FoodsColor.get(i) == 1){
      fill(0,143,252);
    }if (FoodsColor.get(i) == 2){
      fill(250,233,5);
    }
    
    
    if(FoodsX.get(i) + BasicX > BasicX   &&   BasicX - 800 < FoodsX.get(i) + BasicX  &&  FoodsY.get(i) + BasicY > BasicY   &&   BasicY - 800 < FoodsY.get(i) + BasicY){//画面内にある食べ物を選別。
      ellipse(FoodsX.get(i) + BasicX,FoodsY.get(i) + BasicY,10,10);//食べ物を描画
    }
     
  }//ループ終了
  
  
  fill(28,151,255);//プレイヤーの色を設定。
  PlayerMove();//プレイヤー情報の座標を移動。
  ellipse(width/2,height/2,40,40);//プレイヤー描画
  
  
}//関数終了


void keyPressed(){ //キー情報の管理する関数


  if (keyCode == DOWN){
    DownKeyPush = 1;
  }
  if (keyCode == UP){
    UpKeyPush = 1;
  }
  if (keyCode == LEFT){
    LeftKeyPush = 1;
  }
  if (keyCode == RIGHT){
    RightKeyPush = 1;
  }
  
  
}//関数終了


void keyReleased(){ //キー情報の管理する関数


  if(keyCode == UP){
    UpKeyPush = 0;
  }
  if(keyCode == DOWN){
    DownKeyPush = 0;
  }
  if(keyCode == RIGHT){
    RightKeyPush = 0;
  }
  if(keyCode == LEFT){
    LeftKeyPush = 0;
  }
  
  
}//関数終了
